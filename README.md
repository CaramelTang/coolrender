0. 你在看我的仓库诶~ (⊙o⊙)
1. Path Tracing 已经初步完成. Photon Mapping 开学后加上.
2. 注释是啥? 能吃么? 文档是啥? 好吃么? 单元测试是啥? 怎么吃?
3. Debug模式速度巨慢. Release + -Ofast 速度会好一些.
4. 要是有人愿意帮我改成MPI并行, 我会请你吃饭哦~
5. XCode 7 beta版本创建. 欢迎你迁移到其他平台. Apple大法好, 天灭Windows.
7. 代码这么丑, 自己都看不下去了. 我还要努力! (ง •̀_•́)ง
8. 可以做妹子的人肉文档~\\(≧▽≦)/~ 汉子请靠边~ 
9. 用到了TBB做自动并行, CGAL求交点, SOIL做输出, 可以使用homebrew安装.
10. 材料模型似乎有问题, 不过不要在意这些细节...
11. obj文件用maya生成. 网格要三角化, 注意法线.
12. 到处都可能有bug, 欢迎给我发issue!