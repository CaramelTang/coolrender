//
//  PostTreatment.cpp
//  CoolRender
//
//  Created by 唐如麟 on 15/7/27.
//  Copyright © 2015年 唐如麟. All rights reserved.
//

#include <memory.h>

#include "PostTreatment.hpp"
#include "boost/format.hpp"
#include "time.h"
#include "sys/timeb.h"
void
PostTreatment
(const Camera_t & Camera,std::shared_ptr<std::vector<Color_t>> Colors){
    unsigned char * result = new unsigned char[Camera.ResolutionX*Camera.ResolutionY*4];
    double * dresult= new double[Camera.ResolutionX*Camera.ResolutionY*4];
    memset(result,0,Camera.ResolutionX*Camera.ResolutionY*4*sizeof(char));
    memset(dresult,0,Camera.ResolutionX*Camera.ResolutionY*4*sizeof(double));
    
    for(int i=0;i<Colors->size();i++){
        int x=0,y=0;
        int c=Camera.ResamplePixels*2+1;
        y=i/(Camera.ResolutionX*c*c);
        x=(i%(Camera.ResolutionX*c))/c;
        dresult[4*(y*Camera.ResolutionX+x)]+=(*Colors)[i].R;
        dresult[4*(y*Camera.ResolutionX+x)+1]+=(*Colors)[i].G;
        dresult[4*(y*Camera.ResolutionX+x)+2]+=(*Colors)[i].B;
    }
    
    for(int i=0;i<Camera.ResolutionX*Camera.ResolutionY*4;i++){
        dresult[i]/=(Camera.ResamplePixels*2+1)*(Camera.ResamplePixels*2+1);
    }
    
    const double & Aperture = Camera.Aperture;
    for(int y=0;y<Camera.ResolutionY;y++){
        for(int x=0;x<Camera.ResolutionX;x++){
            int temp = dresult[4*(y*Camera.ResolutionX+x)]/Aperture*255;
            result[4*(y*Camera.ResolutionX+x)]= temp > 255 ?255: temp;
            temp = dresult[4*(y*Camera.ResolutionX+x)+1]/Aperture*255;
            result[4*(y*Camera.ResolutionX+x)+1]=temp > 255 ?255: temp;
            temp = dresult[4*(y*Camera.ResolutionX+x)+2]/Aperture*255;
            result[4*(y*Camera.ResolutionX+x)+2]=temp > 255 ?255: temp;
            result[4*(y*Camera.ResolutionX+x)+3]=255;
        }
    }
    
    struct timeb tp;
    struct tm ltime;
    ftime(&tp);
    localtime_r(&tp.time,&ltime);
    std::string OutputName=(boost::format("Output-%1%-%2%--%3%-%4%.bmp")%(ltime.tm_mon+1)%ltime.tm_mday%ltime.tm_hour%ltime.tm_min).str();
    OutputToBMP(OutputName,result,Camera.ResolutionX,Camera.ResolutionY);
    delete [] dresult;
    delete [] result;
    
    system((std::string("open ")+OutputName).data());
}

