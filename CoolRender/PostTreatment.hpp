//
//  PostTreatment.hpp
//  CoolRender
//
//  Created by 唐如麟 on 15/7/27.
//  Copyright © 2015年 唐如麟. All rights reserved.
//

#ifndef PostHandle_cpp
#define PostHandle_cpp

#include <memory>

#include "Camera.hpp"
#include "Color.hpp"
#include "Output.hpp"

void
PostTreatment
(const Camera_t & Camera,std::shared_ptr<std::vector<Color_t>> Colors);

#endif /* PostTreatment_cpp */
