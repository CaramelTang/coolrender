//
//  PTRenderCore.cpp
//  CoolRender
//
//  Created by 唐如麟 on 15/7/23.
//  Copyright © 2015年 唐如麟. All rights reserved.
//

#include <time.h>
#include "tbb/task_scheduler_init.h"
#include "tbb/blocked_range.h"
#include "tbb/parallel_for.h"
#include <CGAL/Origin.h>
#include "DrawScene.hpp"

#include "PTRenderCore.hpp"

#ifdef __DRAW__
std::vector<Segment_3> SegmentsToDraw;
#endif

boost::optional<Intersection_t>
PT::
SelectIntersection_Default
(const Parameters_t & Parameters,const Ray_3 & EyeRay,const std::vector<Intersection_t> & Intersections){
    double mindistance=DBL_MAX;
    boost::optional<Intersection_t> result;
    for(auto & i:Intersections){
        double mindistance0 =CGAL::squared_distance((i).Point, EyeRay.source());
        if((mindistance0<mindistance)  && (mindistance0>EPSILON)) {
            result = i;
            mindistance=mindistance0;
        }
    }
    return result;
}

Ray_3
PT::
CalcMirrorReflectedRay_Default
(const Parameters_t & Parameters,const Ray_3 & EyeRay,const Intersection_t & Intersection){
    const Vector_3 & normal = Intersection.pFace->Normal;
    Vector_3 IncidentVector=EyeRay.to_vector();
    return Ray_3(Intersection.Point,2*(normal*(-IncidentVector))*normal+IncidentVector);
}

Ray_3
PT::
CalcRefractedRay_Default
(const Parameters_t & Parameters,const Ray_3 & EyeRay,const Intersection_t & Intersection){
    Vector_3 reference = Intersection.pFace->Normal;
    Vector_3 Incidentvector=EyeRay.to_vector();
    Incidentvector=NORMALIZE(Incidentvector);
    if(Incidentvector*reference <0) reference=-reference;
    Vector_3 temp=Incidentvector-(Incidentvector*reference)*reference;
    temp=NORMALIZE(temp);
    double angle;
    if(Incidentvector*Intersection.pFace->Normal<0) {
        angle=asin(sin(acos(reference*Incidentvector))/Intersection.pFace->Material.Ni);
    }else{
        angle=asin(sin(acos(reference*Incidentvector))*Intersection.pFace->Material.Ni);
    }
    return Ray_3(Intersection.Point,reference+temp*tan(angle));
}

Ray_3
PT::
CalcDiffuseReflectedRay_Defult
(const Parameters_t & Parameters,const Ray_3 & EyeRay,const Intersection_t & Intersection){
    double a,b,l;
    a=acos(1-2*(((double)(rand()%10000))/9999));
    b=(((double)(random()%10000))/9999)*M_PI*2;
    l=sin(a);
    double x,y,z;
    x=sin(b)*l;
    y=cos(a);
    z=cos(b)*l;
    return Ray_3(Intersection.Point,Vector_3(x,y,z));
}

bool
PT::
IfTotalReflection_Default
(const PT::Parameters_t & Parameters,const Ray_3 & EyeRay,const Intersection_t & Intersection){
    double Ni=Intersection.pFace->Material.Ni;
    assert(Ni>=1);
    Vector_3 IncidentVector=EyeRay.to_vector();
    IncidentVector=NORMALIZE(IncidentVector);
    if(EyeRay.to_vector()*Intersection.pFace->Normal > 0){
        if(Ni*sin(acos(IncidentVector*Intersection.pFace->Normal))>1){
            return true;
        }
    }
    return false;
}

bool
PT::
IfAllowRefraction_Default
(const Parameters_t & Parameters,const Ray_3 & EyeRay,const Intersection_t & Intersection,const int RecursionDeep,bool ShouldTotalReflection){
    const Color_t & Tf=Intersection.pFace->Material.Tf;
    if((Tf.R<EPSILON)&&(Tf.G<EPSILON)&&(Tf.B<EPSILON)) return false;
    if(ShouldTotalReflection && Parameters.IfAllowTotalReflection) return false;
    return true;
}

bool
PT::
IfAllowMirrorReflection_Default
(const Parameters_t & Parameters,const Ray_3 & EyeRay,const Intersection_t & Intersection,const int RecursionDeep,bool ShouldTotalReflection){
    const Color_t & Ks=Intersection.pFace->Material.Ks;
    const Color_t & Tf=Intersection.pFace->Material.Tf;
    if(((Tf.R>EPSILON)||(Tf.G>EPSILON)||(Tf.B>EPSILON)) && ShouldTotalReflection && Parameters.IfAllowTotalReflection ) return true;
    if((Ks.R<EPSILON)&&(Ks.G<EPSILON)&&(Ks.B<EPSILON)) return false;
    return true;
}

bool
PT::
IfAllowDiffuseReflection_Default
(const Parameters_t & Parameters,const Ray_3 & EyeRay,const Intersection_t & Intersection,const int RecursionDeep){
    const Color_t & Kd=Intersection.pFace->Material.Kd;
    if((Kd.R<EPSILON)&&(Kd.G<EPSILON)&&(Kd.B<EPSILON)) return false;
    return true;
};

boost::optional<LightSourceIntersection_t>
PT::
SelectLightSourceIntersection_Default
(const Parameters_t & Parameters,const Ray_3 & EyeRay,const std::vector<LightSourceIntersection_t> & Intersections){
    double mindistance=DBL_MAX;
    boost::optional<LightSourceIntersection_t> result;
    for(auto & i:Intersections){
        double mindistance0=CGAL::squared_distance(i.Point, EyeRay.source());
        if((mindistance0<mindistance)&& (mindistance0>EPSILON))   {
            if(EyeRay.to_vector()*i.pFaceLightSource->Normal<0) result = i;
            mindistance=mindistance0;
        }
    }
    return result;
}

PT::Light_t
PT::
HitLightSource_Default
(const Parameters_t & Parameters,const Ray_3 & EyeRay,const LightSourceIntersection_t & Intersection){
    Vector_3 vector = -EyeRay.to_vector();
    vector=NORMALIZE(vector);
    Light_t result(vector,Intersection.pFaceLightSource->Color*Intersection.pFaceLightSource->Lightness);
#ifdef __DRAW__
    result.isHitLightSource = true;
    result.LightSourceIntersection = Intersection;
#endif
    return result;
}

int
PT::
GetDiffuseReflectionResampleTimes_Default
(const Parameters_t & Parameters,const Ray_3 & EyeRay,const Intersection_t & Intersection,const int RecursionDeep){
    return Parameters.DiffuseReflectionResampleTimes;
}

PT::Light_t
PT::
CalcReturnLight_Default
(const Parameters_t & Parameters,const Ray_3 & EyeRay,const Intersection_t & Intersection,const Light_t & RefractionLight,const std::vector<Light_t> & DiffuseReflectLights,const Light_t & MirrorReflectedLight,bool ShouldTotalReflection){
    Light_t result;
    result.Vector=-NORMALIZE(EyeRay.to_vector());
    const Color_t & Kd = Intersection.pFace->Material.Kd;
    const Color_t & Ks = Intersection.pFace->Material.Ks;
    const Color_t & Tf = Intersection.pFace->Material.Tf;
    
    if(!ShouldTotalReflection){
        result.Color=RefractionLight.Color*Tf+MirrorReflectedLight.Color*Ks;
    }else{
        result.Color=MirrorReflectedLight.Color*(Ks+Tf);
    }
    
    if(DiffuseReflectLights.size()){
        Color_t color;
        for(auto & i:DiffuseReflectLights){
            color+=i.Color;
        }
        color/=DiffuseReflectLights.size();
        result.Color+=color*Kd;
    }
    result.Color*=Intersection.pFace->Material.Ka;
    
    return result;
}

//--------------------------------------------------

PT::Light_t
PT::
PathTracingRecursion
(const Parameters_t & Parameters,const Ray_3 & EyeRay,const int RecursionDeep){
    const LightSources_t & LightSources = *(Parameters.pScene->pLightSources);
    std::vector<LightSourceIntersection_t> LightSourceIntersections = LightSources.QueryIntersections(EyeRay);
    boost::optional<LightSourceIntersection_t> pSelectedLightSourceIntersection = Parameters.SelectLightSourceIntersection(Parameters,EyeRay,LightSourceIntersections);
    
    const Mesh_t & Mesh=*(Parameters.pScene->pMesh);
    std::vector<Intersection_t> Intersections = Mesh.QueryIntersections(EyeRay);
    boost::optional<Intersection_t> pSelectedIntersection=Parameters.SelectIntersection(Parameters,EyeRay,Intersections);
    
    double MinLightSourceIntersectionDistance=DBL_MAX;
    double MinInersectionDistance=DBL_MAX;
    
    if(pSelectedLightSourceIntersection){
        MinLightSourceIntersectionDistance=CGAL::squared_distance(EyeRay.source(), pSelectedLightSourceIntersection->Point);
    }
    if(pSelectedIntersection){
        MinInersectionDistance=CGAL::squared_distance(EyeRay.source(), pSelectedIntersection->Point);
    }
    
    if(MinLightSourceIntersectionDistance<MinInersectionDistance){
        return Parameters.HitLightSource(Parameters,EyeRay,*pSelectedLightSourceIntersection);
    }
    if(MinInersectionDistance<MinLightSourceIntersectionDistance){
        if(RecursionDeep<Parameters.MaxRecursionDeep){
            Light_t RefractedLight;
            Light_t MirrorReflectedLight;
            std::vector<Light_t> DiffuseReflectedLights;
            bool ShouldTotalReflection = Parameters.IfTotalReflection(Parameters,EyeRay,*pSelectedIntersection);
            if(Parameters.IfAllowRefraction(Parameters,EyeRay,*pSelectedIntersection,RecursionDeep,ShouldTotalReflection)){
                Ray_3 RefractedRay=Parameters.CalcRefractedRay(Parameters,EyeRay,*pSelectedIntersection);
                RefractedLight=PathTracingRecursion(Parameters, RefractedRay, RecursionDeep+1);
#ifdef __DRAW__
                SegmentsToDraw.push_back(Segment_3(RefractedRay.source(),RefractedRay.source()+15*NORMALIZE(RefractedRay.to_vector())));
#endif
            }
            if(Parameters.IfAllowMirrorReflection(Parameters,EyeRay,*pSelectedIntersection,RecursionDeep,ShouldTotalReflection)){
                Ray_3 MirrorReflectedRay = Parameters.CalcMirrorReflectedRay(Parameters,EyeRay,*pSelectedIntersection);
                MirrorReflectedLight = PathTracingRecursion(Parameters, MirrorReflectedRay, RecursionDeep+1);
            }
            if(Parameters.IfAllowDiffuseReflection(Parameters,EyeRay,*pSelectedIntersection,RecursionDeep)){
                for(int i=0;i<Parameters.GetDiffuseReflectionResampleTimes(Parameters,EyeRay,*pSelectedIntersection,RecursionDeep);i++){
                    Ray_3 DiffuseReflectedRay = Parameters.CalcDiffuseReflectedRay(Parameters,EyeRay,*pSelectedIntersection);
                    DiffuseReflectedLights.push_back(PathTracingRecursion(Parameters,DiffuseReflectedRay,RecursionDeep+1));
                }
            }
#ifdef __DRAW__
            if(MirrorReflectedLight.LastIntersection.pFace){ //上一次击中了某个表面
                SegmentsToDraw.push_back(Segment_3(MirrorReflectedLight.LastIntersection.Point,pSelectedIntersection->Point));
            }else{
                if(MirrorReflectedLight.isHitLightSource){
                    SegmentsToDraw.push_back(Segment_3(MirrorReflectedLight.LightSourceIntersection.Point,pSelectedIntersection->Point));
                }
            }
#endif
            Light_t result = Parameters.CalcReturnLight(Parameters,EyeRay,*pSelectedIntersection,RefractedLight,DiffuseReflectedLights,MirrorReflectedLight,ShouldTotalReflection);
#ifdef __DRAW__
            MirrorReflectedLight.LastIntersection=*pSelectedIntersection;
#endif
            return result;
        }
    }
    
    return Light_t(NORMALIZE(-EyeRay.to_vector()),Color_t::Black);
}

void
PT::
PathTracingPixel
(const Parameters_t & Parameters,std::shared_ptr<std::vector<Color_t>> result,const int PixelPositionX,const int PixelPositionY){
    const Camera_t & Camera=*(Parameters.pCamera);
    Point_3 PixelPosisiton = Camera.ProjectionPlane[0]+Camera.PixelDistanceX*PixelPositionX*Camera.Right-Camera.PixelDistanceY*PixelPositionY*Camera.Head;
    Ray_3 EyeRay(Camera.Center,PixelPosisiton);
    Light_t resultlight = PathTracingRecursion(Parameters, EyeRay, 0);
#ifdef __DRAW__
    DrawSence(*(Parameters.pScene->pMesh),Parameters.pScene,&Camera,&SegmentsToDraw);
#endif
    (*result)[PixelPositionY*Parameters.pCamera->ResampleResolutionX+PixelPositionX]=resultlight.Color;
}

void
PT::
PathTracingThread
(const Parameters_t & Parameters,std::shared_ptr<std::vector<Color_t>> result,const int PixelPositionY){
    static int finished=0;
    printf("finished = %d / %d\n",finished++,Parameters.pCamera->ResampleResolutionY);
    for(int PixelPositionX=0;PixelPositionX<Parameters.pCamera->ResampleResolutionX;PixelPositionX++)
        PathTracingPixel(Parameters, result, PixelPositionX, PixelPositionY);
}

class
PathTracingThreadClass_t{
public:
    PathTracingThreadClass_t(const PT::Parameters_t & inParameters,std::shared_ptr<std::vector<Color_t>> inresult):Parameters(inParameters),result(inresult){}
    void operator()(const tbb::blocked_range<int> & r)const {
        for(auto PixelPositionY=r.begin();PixelPositionY!=r.end();PixelPositionY++)
            PathTracingThread(Parameters, result, PixelPositionY);
    }
public:
    const PT::Parameters_t & Parameters;
    std::shared_ptr<std::vector<Color_t>> result;
    
};

std::shared_ptr<std::vector<Color_t>>
PT::
PathTracing
(Parameters_t Parameters){
    tbb::task_scheduler_init init;
    std::shared_ptr<std::vector<Color_t>>  result(new std::vector<Color_t>(Parameters.pCamera->ResampleResolutionX*Parameters.pCamera->ResampleResolutionY));
    const Camera_t & Camera=*(Parameters.pCamera);
#ifdef __DRAW__
    PathTracingPixel(Parameters, result,350*5, 200*5);
#endif
    
//        for(int y=0;y<Camera.ResampleResolutionY;y++){
//            for(int x=0;x<Camera.ResampleResolutionX;x++){
//                PathTracingPixel(Parameters, result, x,y);
//            }
//        }
    
    tbb::parallel_for(tbb::blocked_range<int>(0,Camera.ResampleResolutionY), PathTracingThreadClass_t(Parameters,result));
    return result;
}




