//
//  main.cpp
//  CoolRender
//
//  Created by 唐如麟 on 15/7/23.
//  Copyright © 2015年 唐如麟. All rights reserved.
//

#include "Mesh.hpp"
#include "Scene.cpp"
#include "DrawScene.hpp"
#include "LightSource.hpp"
#include "PTRenderCore.hpp"
#include "PostTreatment.hpp"
#include "Material.hpp"

int main(int argc, char ** argv) {

    time_t start = time(NULL);
    
#ifdef __DRAW__
    InitScene(argc, argv);
#endif
    
    Mesh_t Mesh("GlassBall.obj");
    Materials_t Materials("Materials.txt");
    Mesh.SetMaterial("pPlane1", Materials["Diffuse"],Color_t::White);
    Mesh.SetMaterial("pSphere1", Materials["Glass"], Color_t::White);
    LightSources_t LightSources("FaceLightSources.txt");
    
    Camera_t Camera(Point_3(0,20,30),Vector_3(Point_3(0,20,30),Point_3(0,5,0)),Vector_3(0,2,-1),5,800,600,3);
    
    Scene_t Scene(Mesh,LightSources);
    
    PT::Parameters_t Parameters(Scene,Camera);
    Parameters.MaxRecursionDeep =20;
    Parameters.DiffuseReflectionResampleTimes=5;
    
    auto result=PathTracing(Parameters);
    PostTreatment(Camera,result);
    
    std::cout<<"\n此程序的运行时间为"<<time(NULL)-start<<"秒！"<<std::endl;
    
    return 0;
};
