//
//  PTRenderCore.hpp
//  CoolRender
//
//  Created by 唐如麟 on 15/7/23.
//  Copyright © 2015年 唐如麟. All rights reserved.
//

#ifndef RenderCore_cpp
#define RenderCore_cpp

#include <memory>

#include "Camera.hpp"
#include "LightSource.hpp"
#include "Mesh.hpp"
#include "Color.hpp"
#include "Scene.hpp"

namespace PT{
    
    class Parameters_t;
    class Light_t;
    
    boost::optional<Intersection_t>
    SelectIntersection_Default
    (const Parameters_t & Parameters,const Ray_3 & EyeRay,const std::vector<Intersection_t> & Intersections);
    
    boost::optional<LightSourceIntersection_t>
    SelectLightSourceIntersection_Default
    (const Parameters_t & Parameters,const Ray_3 & EyeRay,const std::vector<LightSourceIntersection_t> & Intersections);
    
    Ray_3
    CalcRefractedRay_Default
    (const Parameters_t & Parameters,const Ray_3 & EyeRay,const Intersection_t & Intersection);
    
    Ray_3
    CalcMirrorReflectedRay_Default
    (const Parameters_t & Parameters,const Ray_3 & EyeRay,const Intersection_t & Intersection);
    
    Ray_3
    CalcDiffuseReflectedRay_Defult
    (const Parameters_t & Parameters,const Ray_3 & EyeRay,const Intersection_t & Intersection);
    
    bool
    IfTotalReflection_Default
    (const PT::Parameters_t & Parameters,const Ray_3 & EyeRay,const Intersection_t & Intersection);
    
    bool
    IfAllowRefraction_Default
    (const Parameters_t & Parameters,const Ray_3 & EyeRay,const Intersection_t & Intersection,const int RecursionDeep,bool ShouldTotalReflection);
    
    bool
    IfAllowMirrorReflection_Default
    (const Parameters_t & Parameters,const Ray_3 & EyeRay,const Intersection_t & Intersection,const int RecursionDeep,bool ShouldTotalReflection);
    
    bool
    IfAllowDiffuseReflection_Default
    (const Parameters_t & Parameters,const Ray_3 & EyeRay,const Intersection_t & Intersection,const int RecursionDeep);
    
    Light_t
    HitLightSource_Default
    (const Parameters_t & Parameters,const Ray_3 & EyeRay,const LightSourceIntersection_t & Intersection);
    
    int
    GetDiffuseReflectionResampleTimes_Default
    (const Parameters_t & Parameters,const Ray_3 & EyeRay,const Intersection_t & Intersection,const int RecursionDeep);
    
    Light_t
    CalcReturnLight_Default
    (const Parameters_t & Parameters,const Ray_3 & EyeRay,const Intersection_t & Intersection,const Light_t & RefractionLight,const std::vector<Light_t> & DiffuseReflectLights,const Light_t & MirrorReflectedLight,bool ShouldTotalReflection);
    
    //--------------------------------------------------------
    
    class Light_t{
    public:
        Light_t(const Vector_3 & inVector = CGAL::NULL_VECTOR,const Color_t & inColor = Color_t::Black):Color(inColor),Vector(inVector){};
    public:
        Vector_3 Vector;
        Color_t Color;
#ifdef __DRAW__
        Intersection_t LastIntersection;
        bool isHitLightSource = false;
        LightSourceIntersection_t LightSourceIntersection;
#endif
    };
    
    class Parameters_t{
    public:
        Parameters_t(const Scene_t & inScene,const Camera_t & inCamera):pScene(&inScene),pCamera(&inCamera){}
    public:
        const Scene_t * pScene;
        const Camera_t * pCamera;
        int MaxRecursionDeep=10;
        int DiffuseReflectionResampleTimes=5;
        bool IfAllowTotalReflection = true;
        
        std::function
        <boost::optional<Intersection_t> (const Parameters_t &,const Ray_3 &,const std::vector<Intersection_t> &)>
        SelectIntersection=std::ref(SelectIntersection_Default);
        
        std::function
        <boost::optional<LightSourceIntersection_t> (const Parameters_t & ,const Ray_3 &,const std::vector<LightSourceIntersection_t> &)>
        SelectLightSourceIntersection=std::ref(SelectLightSourceIntersection_Default);
        
        std::function
        <Ray_3 (const Parameters_t &,const Ray_3 &,const Intersection_t &)>
        CalcRefractedRay=std::ref(CalcRefractedRay_Default);
        
        std::function
        <Ray_3 (const Parameters_t &,const Ray_3 &,const Intersection_t &)>
        CalcMirrorReflectedRay=std::ref(CalcMirrorReflectedRay_Default);
        
        std::function
        <Ray_3 (const Parameters_t &,const Ray_3 &,const Intersection_t &)>
        CalcDiffuseReflectedRay=std::ref(CalcDiffuseReflectedRay_Defult);
        
        std::function
        <bool (const PT::Parameters_t &,const Ray_3 &,const Intersection_t &)>
        IfTotalReflection=std::ref(IfTotalReflection_Default);
        
        std::function
        <bool (const Parameters_t &,const Ray_3 &,const Intersection_t &,const int,bool)>
        IfAllowRefraction=std::ref(IfAllowRefraction_Default);
        
        std::function
        <bool (const Parameters_t &,const Ray_3 &,const Intersection_t &,const int,bool)>
        IfAllowMirrorReflection=std::ref(IfAllowMirrorReflection_Default);
        
        std::function
        <bool (const Parameters_t &,const Ray_3 &,Intersection_t &,const int)>
        IfAllowDiffuseReflection=std::ref(IfAllowDiffuseReflection_Default);
        
        std::function
        <Light_t (const Parameters_t &,const Ray_3 &,const LightSourceIntersection_t &)>
        HitLightSource=std::ref(HitLightSource_Default);
        
        std::function
        <int (const Parameters_t &,const Ray_3 &,const Intersection_t &,const int)>
        GetDiffuseReflectionResampleTimes=std::ref(GetDiffuseReflectionResampleTimes_Default);
        
        std::function
        <Light_t (const Parameters_t &,const Ray_3 &,const Intersection_t &,const Light_t &,const std::vector<Light_t> &,const Light_t &,bool)>
        CalcReturnLight=std::ref(CalcReturnLight_Default);
    };
    
    Light_t
    PathTracingRecursion
    (const Parameters_t & Parameters,const Ray_3 & EyeRay,const int RecursionDeep);
    void
    PathTracingPixel
    (const Parameters_t & Parameters,std::shared_ptr<std::vector<Color_t>> result,const int PixelPositionX,const int PixelPositionY);
    void
    PathTracingThread
    (const Parameters_t & Parameters,std::shared_ptr<std::vector<Color_t>> result,const int PixelPositionY);
    std::shared_ptr<std::vector<Color_t>>
    PathTracing
    (const Parameters_t Parameters);
    
}
#endif /* PTRenderCore_cpp */
